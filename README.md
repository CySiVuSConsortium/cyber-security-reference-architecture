In order to view and edit the model of the reference architecture, the open source tool *Archi* is required.  This can be obtained free of charge from the following web page: [Archi](https://www.archimatetool.com). 

Since *Archi* is not able to clone models from a repository by default, a plugin is also necessary. Please follow the detailed installation instructions provided under the following link [Archi Plugin](https://github.com/archimatetool/archi-modelrepository-plugin/wiki). If the installation was successful, the Collaboration option should appear in the menu bar of *Archi*. In addition to the plugin, a *GitLab* account is mandatory for cloning the repository. The GitLab account is also free of charge and can be created on the [GitLab](https://gitlab.com/) page.

To clone the reference architecture from the repository, choose **Collaboration** -> **Import Remote Model to Workspace**.

![images_step_1](/uploads/4394e11b861c1b8a3e3ac2ef8ee44f2e/images_step_1.png)

As URL use the HTTPS Link from the repository (https://gitlab.com/CySiVuSConsortium/cyber-security-reference-architecture.git). Enter the username and password of your GitLab account. No special permissions are required. If you entered the necessary data, click **OK** and the Plugin will start fetching the reference architecture from the repository.

![images_step_2](/uploads/8698d93f03b049019189f9a8f9c785ef/images_step_2.png)

If the fetching was successfully finished, you should now see the model *V2x unified model* among your available models. All created items can be accessed in folders according to their level (e.g. Business Level). The Folder *Views* contains the views for the different communication categories.

You can't commit anything to the repository. If you want to extend the model, please create your own repository where you can clone the existing reference architecture.

The project is CC BY-SA 4.0 licensed. For details please refer to the *LICENSE* file. A current version of the license text can be found at https://creativecommons.org/licenses/by-sa/4.0/.

The security overlay used in the architecture is taken from the following publication:


*Modeling enterprise risk management and security with the ArchiMate language*. / Band, Iver; Engelsman, Wilco; Feltus, Christophe; González Paredes, Sonia; Hietala, Jim; Jonkers, Henk; Massart, Sébastien.
42 p. 2015, A White Paper Published by The Open Group.




